var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);

app.get('/', function(req, res){
  res.sendFile(__dirname + '/index.html');
});

io.on('connect', function(socket){
  
  console.log('User connected' );
  socket.broadcast.emit('connect', 'User connected');
  
  socket.on('disconnect', function(msg){
    console.log(msg);
    io.emit('disconnect', 'User disconnected');
  });
  socket.on('connect', function(msg){
    console.log(msg);
    
  });
  
  socket.on('chat message', function(msg){
    console.log('message: ' + msg);
    io.emit('chat message', msg);
  });
  
      
});



http.listen(3001, function(){
  console.log('listening on *:3001');
});
